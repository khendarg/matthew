#!/usr/bin/env python

from __future__ import print_function, division, unicode_literals

from Bio import Entrez
import argparse
import os
import sys

def list2csv(l):
	s = ''
	for item in l: s += str(item) + ','
	return s[:-1]

def get_taxons(acclist, email, chunksize=100):
	Entrez.email = email
	Entrez.tool = 'acc2tax'

	taxids = []
	for i in range(0, len(acclist), chunksize):
		handle = Entrez.elink(dbfrom='protein', db='taxonomy', id=list2csv(acclist[i:i+chunksize]))
		record = Entrez.read(handle)
		handle.close()
		for tax in record[0]['LinkSetDb'][0]['Link']:
			taxids.append(tax['Id'])

	taxons = []
	for i in range(0, len(taxids), chunksize):
		handle = Entrez.efetch(db='taxonomy', id=list2csv(taxids[i:i+chunksize]))
		record = Entrez.read(handle)
		handle.close()
		for i, tax in enumerate(record):
			#print('\t', tax['Lineage'])
			taxon = []
			for lev in tax['LineageEx']:
				taxon.append(lev['ScientificName'])
			taxons.append(taxon)
	return taxons

def main(infile, outfile, email, chunksize=100, skip=0):
	acclist = []
	with open(infile) as f:
		for l in f:
			acclist.append(l.strip())

	#acclist = acclist[0:100]

	if skip: f = open(outfile, 'a')
	else: f = open(outfile, 'w')

	for i in range(0, len(acclist), chunksize):
		if i < skip: continue
		print('working with {} starting at {}'.format(infile, i), file=sys.stderr)
		taxlist = get_taxons(acclist[i:i+chunksize], email)
		for tax in taxlist:
			s = ''
			for lev in tax: s += lev + '\t'
			f.write(s.strip() + '\n')
		f.flush()
	f.close()
		

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	if 'ENTREZ_EMAIL' in os.environ: parser.add_argument('-e', default=os.environ['ENTREZ_EMAIL'], help='(default:{})'.format(os.environ['ENTREZ_EMAIL']))
	else: parser.add_argument('-e')

	parser.add_argument('-k', help='skip a few')

	parser.add_argument('infile', nargs='?', default='/dev/stdin')
	parser.add_argument('outfile', nargs='?', default='/dev/stdout')

	args = parser.parse_args()
	
	if not args.e: raise TypeError('Need an email (-e or set $ENTREZ_EMAIL)')

	main(args.infile, args.outfile, email=args.e, skip=args.k)
